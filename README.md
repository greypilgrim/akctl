# akctl

akctl is a simple tool to associate Azure Kubernetes Service (AKS) subscriptions with kubectl contexts.
It is useful for folks that often switch between Kubernetes contexts involving different Azure
subscriptions.

Renewing your AKS credentials **without akctl**:
1. Figure out what Azure resource group and subscription your cluster is in
1. Switch your az CLI session to the respective subscription: `az account set -s $subscription`
1. Finally, renew your credentials with `az aks get-credentials --resource-group $rg --name $name`

Renewing your AKS credentials **with akctl**:
1. `akctl getcred`
1. Profit

## Features

### Core
- Compatible with non-AKS Kubernetes contexts
- Run `az cli get-credentials` and record the subscription and kubeconfig context names
- Compatible with $KUBECONFIG environment setting
- Optionally supply answer file
- Build answer file from nothing or add/modify supplied answer file
  (Detect if answer file is read-only)
- Supplied answer file can be local or remote

### Maybe
- Work with k8s pxe auth
- First-run setup wizard to supply answer file and any necessary credentials, or
- No configuration necessary to get running

## Version Requirements
- az cli
- kubectl
- golang

## Development
- [ ] Supply sample answer file
- [ ] gitignore answer files
