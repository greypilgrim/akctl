# Roadmap

## Phase 1
Ability to execute `az` and `kubectl` commands reliably.

## Phase 2
akctl will read/write AKS subscription and cluster details to ini file on user's
local computer, maybe $HOME directory.

## Phase 3
akctl can optionally read/write cluster details to remote location (probably Azure
storage account) instead of local ini file.
